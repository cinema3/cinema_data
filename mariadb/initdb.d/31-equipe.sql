create table if not exists `equipe` (
  `film` uuid not null,
  `personne` uuid not null,
  `role` varchar(25) default null,
  `alias` varchar(255) default null,
  primary key (`film`,`personne`),
  key `film` (`film`),
  key `personne` (`personne`),
  constraint `equipe_film_fk` foreign key (`film`) references `film` (`id`),
  constraint `equipe_personne_fk` foreign key (`personne`) references `personne` (`id`)
) engine=innodb;
