create table if not exists `site` (
  `id` int(11) not null auto_increment,
  `titre` varchar(255) not null,
  `url` varchar(255) not null,
  primary key (`id`)
) engine=innodb;

load data infile '/tmp/42-site.csv'
replace into table `site`
fields terminated by ','
lines terminated by '\n'
ignore 1 lines;


create table if not exists `reference` (
  `id` uuid not null,
  `site` int(11) not null,
  `identifiant` varchar(50) not null,
  primary key (`id`,`site`)
) engine=innodb;
