drop view if exists `acteur`;
create algorithm=undefined definer=`root`@`localhost` sql security definer view `acteur` as
select `personne`.`id` as `id`,coalesce(`personne`.`artiste`,concat(`personne`.`prenom`,' ',`personne`.`nom`)) as `nom`,
`personne`.`naissance` as `naissance`,
`personne`.`deces` as `deces`,
coalesce(`personne`.`deces`, `personne`.`deces` - `personne`.`naissance`),
CASE
    WHEN (`personne`.deces IS NULL) then TIMESTAMPDIFF(YEAR, `personne`.`naissance`, CURDATE())
    ELSE TIMESTAMPDIFF(YEAR, `personne`.`naissance`, `personne`.`deces`)
  END AS age,
`personne`.`nationalite` as `nationalite`,
count(`film`.`id`) as `nbFilm`
from ((`personne`
  join `equipe` on(`equipe`.`personne` = `personne`.`id`))
  left join `film` on(`film`.`id` = `equipe`.`film`))
where `equipe`.`role` = 'acteur'
group by 1,2,3,4,5;

drop view if exists `film_detail`;
create algorithm=undefined definer=`root`@`localhost` sql security definer view `film_detail` as
select film.titre, film.titre_original, film.annee, film.sortie, film.duree, serie.serie
, resume.texte,
(select JSON_ARRAYAGG(`cinema`.`genre`.`genre`) from `cinema`.`film_genre`
  inner join genre on (genre.id = film_genre.genre)
  where `cinema`.`film_genre`.`film` =  `cinema`.`film`.`id`) as genre
from film
left join serie on (film.serie = serie.id)
left join `resume` on (film.id = resume.film and resume.langue = 'fra');

drop table if exists `seance_detail`;
create algorithm=undefined definer=`root`@`localhost` sql security definer view `seance_detail` as
SELECT seance.id, salle.etablissement, etablissement.nom, seance.seance, film.titre FROM `seance`
INNER JOIN salle on (seance.salle = salle.id) INNER JOIN etablissement on (etablissement.id = salle.etablissement)
INNER JOIN film ON (film.id=seance.film);
