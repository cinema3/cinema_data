drop table if exists certification;

create table if not exists certification
(
  `id` integer,
  `pays` char(2),
  `certification` varchar(10),
  `description` text,
  `ordre` tinyint
);

load data infile '/tmp/46-certification.csv'
replace into table `certification`
fields terminated by ',' enclosed by '"'
lines terminated by '\n'
ignore 1 lines
(pays,ordre,certification,description);
