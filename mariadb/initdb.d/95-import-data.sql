


load data infile '/tmp/31-equipe.csv'
replace into table equipe
fields terminated by ','
lines terminated by '\n'
ignore 1 lines
(film, personne, role, @alias)
set alias = nullif(@alias,'');

-- Genres

load data infile '/tmp/41-film_genre.csv'
replace into table film_genre
fields terminated by ',' enclosed by '\"'
lines terminated by '\n'
ignore 1 lines;

load data infile '/tmp/41-production.csv'
replace into table production
fields terminated by ','
lines terminated by '\n'
ignore 1 lines;


-- Externes

load data infile '/tmp/42-reference.csv'
replace into table `reference`
fields terminated by ',' enclosed by '\"'
lines terminated by '\n'
ignore 1 lines;



-- Séances

DELIMITER //

FOR i IN 1..1000
DO
  INSERT INTO seance (film, salle, seance)
  VALUES ((SELECT id FROM film WHERE RAND() > 0.9 ORDER BY RAND() LIMIT 1)
    , (FLOOR(RAND()*77)+1)
    , (CURRENT_DATE + INTERVAL FLOOR(RAND() * 14) DAY + INTERVAL 21 hour));
END FOR;
//

DELIMITER ;
