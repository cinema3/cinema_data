drop table if exists `resume`;

create table if not exists `resume` (
  `film` uuid not null,
  `langue` char(3) not null,
  `texte` text not null
) engine = innodb;

ALTER TABLE `resume` ADD PRIMARY KEY (`film`,`langue`);
alter table `resume` ADD CONSTRAINT `resume_film_fk` FOREIGN KEY (`film`) REFERENCES `film`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
alter table `resume` ADD CONSTRAINT `resume_langue_fk` FOREIGN KEY (`langue`) REFERENCES `i18n`.`langue`(`code`) ON DELETE RESTRICT ON UPDATE RESTRICT;

alter table `resume` add fulltext key `resume_texte` (`texte`);

load data infile '/tmp/44-resume.csv'
replace into table `resume`
fields terminated by ',' enclosed by '\"'
lines terminated by '\n'
ignore 1 lines;
