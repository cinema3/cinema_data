create table if not exists `film_genre` (
  `film` uuid not null,
  `genre` int(11) not null,
  primary key (`film`,`genre`),
  constraint `film_genre_ibfk_1` foreign key (`genre`) references `genre` (`id`),
  constraint `film_genre_ibfk_2` foreign key (`film`) references `film` (`id`)
) engine=innodb;


create table if not exists `production` (
  `film` uuid not null,
  `societe` uuid not null
) engine=innodb;
