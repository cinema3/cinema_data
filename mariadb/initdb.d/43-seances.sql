create table if not exists `salle` (
  `id` int(11) not null auto_increment,
  `etablissement` int(11) not null,
  `salle` varchar(20) not null,
  `sieges` int(11) not null,
  primary key (`id`),
  key `salle_ibfk_1` (`etablissement`),
  constraint `salle_ibfk_1` foreign key (`etablissement`) references `etablissement` (`id`)
) engine=innodb;


load data infile '/tmp/43-salle.csv'
replace into table `salle`
fields terminated by ','
lines terminated by '\n'
ignore 1 lines
(etablissement, salle, sieges);


create table if not exists `seance` (
  `id` int(11) not null auto_increment,
  `film` uuid not null,
  `salle` int(11) not null,
  `seance` datetime not null,
  primary key (`id`),
  key `seance_ibfk_1` (`film`),
  key `seance_ibfk_2` (`salle`),
  constraint `seance_ibfk_1` foreign key (`film`) references `film` (`id`),
  constraint `seance_ibfk_2` foreign key (`salle`) references `salle` (`id`)
) engine=innodb;
