use `cinema`;

drop table if exists personne;

create table if not exists `personne` (
  `id` uuid not null default uuid(),
  `nom` varchar(50) not null,
  `prenom` varchar(50) not null,
  `naissance` date default null,
  `deces` date default null,
  `artiste` varchar(50) default null,
  `nationalite` char(2) default null,
  `photo` varchar(50) default null,
  `created_at` timestamp not null default current_timestamp(),
  `updated_at` timestamp null default null on update current_timestamp(),
  primary key (`id`),
  key `personne_nom_idx` (`nom`)
) engine=innodb;

load data infile '/tmp/10-personne.csv'
replace into table personne
fields terminated by ','
lines terminated by '\n'
ignore 1 lines
(id, nom, prenom, naissance, @deces, nationalite, @artiste, @photo)
set
deces = nullif(@deces,'')
,artiste = nullif(@artiste,'')
,photo = nullif(@photo,'');
