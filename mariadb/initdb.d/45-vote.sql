drop table if exists `vote`;

create table if not exists `vote` (
  `id` int(11) not null primary key auto_increment,
  `film` uuid not null,
  `user` uuid not null,
  `note` integer unsigned not null
) engine=innodb;


-- alter table `vote` add primary key (`film`,`user`);

alter table `vote`
  add constraint `vote_film_fk` foreign key (`film`) references `film` (`id`),
  add constraint `vote_user_fk` foreign key (`user`) references `auth`.`user` (`id`),
  add constraint `vote_note` check (note < 6);

create unique index vote_film_user
  on vote(film, user);


drop procedure if exists vote_calcul;

DELIMITER //
create definer=`root`@`localhost` PROCEDURE vote_calcul(IN film uuid)
READS SQL DATA
BEGIN
  SELECT count(*), avg(note) INTO @votants, @moyenne FROM `vote` WHERE film = film;
  UPDATE film SET vote_votants=COALESCE(@votants,0), vote_moyenne=COALESCE(@moyenne,0) WHERE id = film;
END//
DELIMITER ;

DELIMITER //
CREATE TRIGGER vote_calcul_insert
AFTER INSERT ON vote
FOR EACH ROW
BEGIN
  CALL vote_calcul(NEW.film);
END; //
DELIMITER ;


DELIMITER //
CREATE TRIGGER vote_calcul_update
AFTER UPDATE ON vote
FOR EACH ROW
BEGIN
  CALL vote_calcul(NEW.film);
END; //
DELIMITER ;


DELIMITER //
CREATE TRIGGER film_vote_update
BEFORE UPDATE ON film
FOR EACH ROW
BEGIN
  SET NEW.vote_votants = COALESCE(OLD.vote_votants,0) + 1;
  SET NEW.vote_moyenne = COALESCE(OLD.vote_moyenne,0) + (NEW.vote_moyenne - COALESCE(OLD.vote_moyenne,0)) / NEW.vote_votants;
END; //
DELIMITER ;

-- https://stackoverflow.com/questions/1318224/mysql-fire-trigger-for-both-insert-and-update
