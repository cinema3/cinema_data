drop table if exists etablissement;

create table if not exists `etablissement` (
  `id` int(11) not null,
  `nom` varchar(60) default null,
  `voie` varchar(60) default null,
  `codepostal` varchar(8) default null,
  `ville` varchar(40) default null,
  `coordonnees` point not null,
  `created_at` timestamp not null default current_timestamp(),
  `updated_at` timestamp null default null on update current_timestamp(),
  primary key (`id`),
  spatial key `idx_coordonnees` (`coordonnees`)
) engine=innodb default charset=utf8mb4 collate=utf8mb4_general_ci;

load data infile '/tmp/cnc-données-cartographie-2021.csv'
replace into table etablissement
fields terminated by ',' enclosed by '\"'
lines terminated by '\n'
ignore 1 lines
(
  @nauto,
  nom,
  @région_administrative,
  @adresse,
  @code_insee,
  @commune,
  @population,
  @dep,
  @nuu,
  @unité_urbaine,
  @population_unité_urbaine,
  @situation_géographique,
  @écrans,
  @fauteuils,
  @semaines_activité,
  @séances,
  @entrées_2021,
  @entrées_2020,
  @évolution_entrées,
  @tranche_entrées,
  @propriétaire,
  @programmateur,
  @ae,
  @catégorie_art_et_essai,
  @label_art_et_essai,
  @genre,
  @multiplexe,
  @zone_commune,
  @nombre_films_programmés,
  @nombre_films_inédits,
  @nombre_films_en_semaine_1,
  @pdm_films_français,
  @pdm_films_américains,
  @pdm_films_européens,
  @pdm_autres_films,
  @films_art_et_essai,
  @part_séances_films_art_et_essai,
  @pdm_films_art_et_essai,
  @latitude,
  @longitude
)
set
id = @nauto,
voie = @adresse,
ville = @commune,
coordonnees = point(@latitude, @longitude);
