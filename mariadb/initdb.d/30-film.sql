drop table if exists serie;

create table if not exists `serie` (
  `id` int(11) not null auto_increment,
  `serie` varchar(30) not null,
  primary key (`id`)
) engine=innodb;

load data infile '/tmp/41-serie.csv'
replace into table serie
fields terminated by ','
lines terminated by '\n'
ignore 1 lines;


drop table if exists film;

create table if not exists `film` (
  `id` uuid not null default uuid(),
  `titre` varchar(80) not null,
  `titre_original` varchar(80) default null,
  `annee` int(11) default null,
  `sortie` date default null,
  `duree` int(11) default null,
  `serie` int(11) default null,
  `vote_votants` integer unsigned,
  `vote_moyenne` decimal(4,2),
  `created_at` timestamp not null default current_timestamp(),
  `updated_at` timestamp null default null on update current_timestamp(),
  primary key (`id`),
  key `serie` (`serie`),
  constraint `film_ibfk_1` foreign key (`serie`) references `serie` (`id`)
) engine=innodb;

load data infile '/tmp/30-film.csv'
replace into table film
fields terminated by ',' enclosed by '\"'
lines terminated by '\n'
ignore 1 lines
(id, titre, titre_original, annee, sortie, duree, @serie)
set
serie = nullif(@serie,'');
