drop table if exists `film_sans_equipe`;
create algorithm=undefined definer=`root`@`localhost` sql security definer view `film_sans_equipe` as select `film`.`titre` as `titre`,count(`equipe`.`film`) as `nb` from (`film` left join `equipe` on(`equipe`.`film` = `film`.`id`)) group by `film`.`titre` having count(`equipe`.`film`) = 0;

drop table if exists `film_sans_resume`;
create algorithm=undefined definer=`root`@`localhost` sql security definer view `film_sans_resume` as
select `film`.`id`, `film`.`titre` as `titre`,
count(`resume`.`film`) as `nb`
from (`film` left join `resume` on(`resume`.`film` = `film`.`id`))
group by `film`.`titre` having count(`resume`.`film`) = 0;



drop table if exists `personne_sans_role`;
create algorithm=undefined definer=`root`@`localhost` sql security definer view `personne_sans_role` as select `personne`.`id` as `id`,`personne`.`prenom` as `prenom`,`personne`.`nom` as `nom`,count(`equipe`.`personne`) as `nb` from (`personne` left join `equipe` on(`equipe`.`personne` = `personne`.`id`)) group by 1,2,3 having count(`equipe`.`personne`) = 0;



grant select, show view on `i18n`.* to 'cinema';
grant select, show view on `auth`.* to 'cinema';
