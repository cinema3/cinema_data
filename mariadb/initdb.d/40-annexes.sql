-- Genres

create table if not exists `genre` (
  `id` int(11) not null,
  `genre` varchar(30) not null,
  primary key (`id`)
) engine=innodb;

load data infile '/tmp/40-genre.csv'
replace into table genre
fields terminated by ',' enclosed by '\"'
lines terminated by '\n'
ignore 1 lines;

-- Sociétés de production

create table if not exists `societe` (
  `id` uuid not null,
  `societe` varchar(35) not null,
  primary key (`id`)
) engine=innodb;

load data infile '/tmp/40-societe.csv'
replace into table societe
fields terminated by ','
lines terminated by '\n'
ignore 1 lines;
