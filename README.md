# cinema_data

## Description
Données brutes pour l'API Cinéma. Liste des acteurs, films et établissements de cinéma

## Sources

CNC Centre National du Cinéma et de l'Image Animée

https://www.data.gouv.fr/fr/datasets/liste-des-etablissements-cinematographiques-actifs-1/

Liste des visas d'exploitations cinématographiques délivrés de 1945 à 2020
